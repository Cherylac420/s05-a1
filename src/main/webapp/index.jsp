<!-- Imports in JSP can be achieved by using directives -->
<!-- Directives are defined by the "%@" symbol which is used to define page attributes -->
<!-- Multiple import packages can be achieved by adding a comma (e.g. import="java.util.Date", java.util.*-->
<%@ page language="java"
	contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
	import="java.util.Date"
 	%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Java Server Pages</title>
</head>
<body>
	<h1>Welcome to Hotel Servlet</h1>
	<!-- JSP allows integration of HTML tags with java syntax -->
	
	<!-- Prints a message in the console -->
	<% System.out.println("Hello from JSP"); %>
	
	<!-- Creates a variable currentDateTime -->
	<%!
		Date currentDateTime = new java.util.Date();o
	%>  
	
	<p>The time now is <%= currentDateTime %></p>
	
	<!-- JSP declaration -->
	<!-- Allows declaration one or more variables and methods -->
	<%! 
		private int initVar = 0;
		private int serviceVar = 0;
		private int destroyVar = 0;
	%>
	
	<!-- JSP method Declaration -->
	<%! 
		public void jspInit(){
			initVar++;
			System.out.println("jspInit(): init"+initVar);
		}

		public void jspDestroy(){
			destroyVar++;
			System.out.println("jspDestroy(): destroy"+destroyVar);
		}
	%>
	
	<!-- JSP Scriplets -->
	<!-- allows any Java language statement, variable, method declaration and expression  -->
	<%
		serviceVar++;
		System.out.println("jspService(): service" + serviceVar);
		String content1 = "intialization  (content1): " + initVar;
		String content2 = "service (content2) : " + serviceVar;
		String content3 = "destroy (content3): " + destroyVar;
	%>
	
	<!-- JSP expressions  -->
	<!-- Code placed within the JSP expression tag is written to the output stream of the response -->
	<!-- out.println is no longer required to print values of variables/methods-->
	<h1>JSP Expressions</h1>
	<p><%=content1 %></p>
	<p><%=content2 %></p>
	<p><%=content3 %></p>
	
	<h1>Create an account</h1>
	<form action="user" method="post">
	
		<label for="firstName">First Name: </label>
		<input type="text" name="firstName"> <br>
		
		<label for="lastName">Last Name: </label>
		<input type="text" name="lastName"> <br>

		<label for="email"> Email </label>
		<input type="email" name="email"> <br>
		
		<label for="contact">Contact: </label>
		<input type="text" name="contact"> <br>
		
		<input type="submit">
	</form>	
	
</body>
</html>
